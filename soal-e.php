<html>

<head>
    <?php
    include 'dbconfig.php';
    $bulan_awal = 6;
    $bulan_akhir = 9;
    $rangeBulan = range($bulan_awal, $bulan_akhir);
    $query = "SELECT month(created_at) as bulan, SUM(total_payment) as pendapatan FROM `orders` WHERE month(created_at) BETWEEN '$bulan_awal' AND '$bulan_akhir' GROUP BY bulan";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_all($result);

    ?>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Bulan', 'Pendapatan', {
                    role: 'annotation'
                }],


                <?php
                for ($i = 0; $i < count($row); $i++) {
                    $monthNum  = $row[$i][0];
                    $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                    $monthName = $dateObj->format('F');
                    if ($i == 0) {
                        echo "['{$monthName}', {$row[$i][1]},'-'],";
                    } else {
                        $persentase = ($row[$i][1] - $row[$i - 1][1]) / $row[$i - 1][1] * 100;
                        echo "['{$monthName}', {$row[$i][1]},'{$persentase}%'],";
                    }
                }

                ?>

            ]);

            var options = {
                title: 'persentase kenaikan pendapatan',
                curveType: 'function',
                legend: {
                    position: 'bottom'
                }
            };

            var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

            chart.draw(data, options);
        }
    </script>
</head>

<body>
    <h1>pendapatan dari bulan juni sampai agustus</h1>

    <div id="curve_chart" style="width: 900px; height: 500px"></div>
    <p>(bulan agustus tidak ada transaksi)</p>
</body>

</html>