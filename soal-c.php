<html>

<head>
    <?php
    include 'dbconfig.php';
    $bulan_awal = 6;
    $bulan_akhir = 9;
    $query = "SELECT month(created_at) as bulan, date(created_at) as tanggal, COUNT(*) as total, SUM(total_payment) as nominal FROM `orders` WHERE month(created_at) BETWEEN '{$bulan_awal}' AND '{$bulan_akhir}' GROUP BY bulan,tanggal";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_all($result);
    //data total transaksi dalam range tanggal
    $total_transaksi = array_sum(array_column($row, 2));
    //range bulan
    $rangeBulan = range($bulan_awal, $bulan_akhir);
    //pecah data array menjadi array perbulan sesuai range bulan
    foreach ($rangeBulan as $b) {
        $bulan[$b] = array_filter($row, function ($value) use ($b) {
            return $value[0] == $b;
        });
    }
    //mencari tanngal dengan transaksi terbanyak perbulan
    foreach ($rangeBulan as $range) {
        foreach ($bulan[$range] as $key) {
            if ($key[2] == max(array_column($bulan[$range], 2))) {
                $dataTanggalTransaksiTertinggi[$range] = $key;
            }
        }
    }

    ?>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {
            'packages': ['table']
        });
        google.charts.setOnLoadCallback(drawTable);

        function drawTable() {
            var data = new google.visualization.DataTable();
            data.addColumn('number', 'Bulan');
            data.addColumn('string', 'Tanggal dengan transaksi terbanyak');
            data.addColumn('number', 'Jumlah Transaksi');
            data.addColumn('number', 'nominal Transaksi');
            data.addRows([
                <?php
                foreach ($rangeBulan as $b) {
                    if (array_key_exists($b, $dataTanggalTransaksiTertinggi)) {
                        # code...
                        echo "[{$b}, '{$dataTanggalTransaksiTertinggi[$b][1]}', {$dataTanggalTransaksiTertinggi[$b][2]}, {$dataTanggalTransaksiTertinggi[$b][3]}],";
                    } else {
                        echo "[{$b}, '-', 0, 0],";
                    }
                }

                ?>
            ]);

            var table = new google.visualization.Table(document.getElementById('table_div'));

            table.draw(data, {
                showRowNumber: true,
                width: '50%',
                height: '50%'
            });
        }
    </script>
</head>

<body>
    <div>
        <p>Total Transaksi Bulan <?= $bulan_awal; ?> sampai <?= $bulan_akhir; ?> adalah <?= $total_transaksi; ?></p>
        <div id="table_div"></div>
    </div>
</body>

</html>