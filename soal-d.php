<html>

<head>
    <?php
    include 'dbconfig.php';
    $query = "SELECT p.id,p.name , COUNT(o.product_id) AS total FROM `products` p LEFT JOIN `order_items` o ON p.id = o.product_id GROUP BY p.name,p.id ORDER BY total DESC LIMIT 10;";
    $result = mysqli_query($con, $query);


    ?>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {
            'packages': ['bar']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Nama Produk', 'jumlah penjualan', ],
                <?php
                while ($row = mysqli_fetch_array($result)) {
                    extract($row);
                    echo "['{$name}', {$total}],";
                }
                ?>
            ]);

            var options = {
                chart: {
                    title: '10 Produk Terlaris',
                    subtitle: '',
                },
                bars: 'horizontal' // Required for Material Bar Charts.

            };

            var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        }
    </script>
</head>

<body>
    <div id="columnchart_material" style="width: 90vw; height: 500px;"></div>
</body>

</html>