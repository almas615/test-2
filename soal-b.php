<html>

<head>
    <?php
    include 'dbconfig.php';
    $query = "SELECT c.name,o.total_payment FROM customers c LEFT JOIN `orders` o ON c.id = o.customer_id WHERE (month(o.created_at) BETWEEN '06' AND '09') AND  o.total_payment >= 150000 GROUP BY c.name,o.total_payment";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_all($result);

    ?>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {
            'packages': ['table']
        });
        google.charts.setOnLoadCallback(drawTable);

        function drawTable() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Nama');
            data.addColumn('number', 'nominal Transaksi');
            data.addRows([
                <?php
                foreach ($row as $r) {
                    echo "['" . $r[0] . "', " . $r[1] . "],";
                }

                ?>
            ]);

            var table = new google.visualization.Table(document.getElementById('table_div'));

            table.draw(data, {
                showRowNumber: true,
                width: '50%',
                height: '50%'
            });
        }
    </script>
</head>

<body>
    <div>
        <p>Customer dengan transaksi diatas 150000 dari bulan 6 sampai 9</p>
        <div id="table_div"></div>
    </div>
</body>

</html>